from django.contrib import admin
from django.contrib.admin import ModelAdmin
from Wancho.models import *


class VideojocAdmin(ModelAdmin):
    pass

class PlataformaAdmin(ModelAdmin):
    pass

admin.site.register(Videojoc, ModelAdmin)
admin.site.register(Plataforma, ModelAdmin)
# Register your models here.
