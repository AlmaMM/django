from django.http import HttpRequest, HttpResponse
from django.shortcuts import render, get_object_or_404,redirect
from django.http import Http404
from django.contrib.auth import logout
from django.contrib.auth.mixins import LoginRequiredMixin
# Create your views here.
from django.views.generic.base import View
from Wancho.models import *


class Wancho(View):
    # Definim el mètode HTTP el qual s'ha d'atendre
    def get(self, data):
        return HttpResponse(content='Això és una prova')


class Admin(View):
    # Definim el mètode HTTP el qual s'ha d'atendre
    def get(self, request):
        context = {
            'videojocs': list(Videojoc.objects.all()),
            'plataformes': list(Plataforma.objects.all())
        }
        return render(request, 'menuAdmin.html', context=context)
