from django.db import models

class Plataforma (models.Model):
    Nom = models.CharField(max_length=30, null=False, default="")
    Data = models.DateTimeField()
    models.Model.do_not_call_in_templates = True
    def __str__(self):
        return self.Nom

class Videojoc(models.Model):
    Nom=models.CharField(max_length=30, null=False, default="")
    Preu = models.IntegerField(default=0)
    Nou = models.BooleanField(default=True)
    Plataforma = models.ForeignKey(Plataforma, on_delete=models.CASCADE)
    models.Model.do_not_call_in_templates = True
    def __str__(self):
        return self.Nom

class Usuari(models.Model):
    Nom=models.CharField(max_length=30, null=False, default="")
    Plataforma=models.ManyToManyField(Plataforma)
    Videojoc=models.ManyToManyField(Videojoc)
    models.Model.do_not_call_in_templates = True
    def __str__(self):
        return self.Nom
# Create your models here.
