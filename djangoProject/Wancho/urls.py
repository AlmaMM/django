from django.urls import path, include
from . import views
from Wancho.views import *

app_name = 'Wancho'

urlpatterns = [
    path('', Wancho.as_view()),
    path('admin/', Admin.as_view())
]